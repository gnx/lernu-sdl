# Научи SDL

Тук записвам напредъка си, изучавайки програмирането на игри на C и C++ под ГНУ+Линукс, ползвайки библиотеката SDL.

За основа ползвам плейлиста с видеа ["SDL2 Simple Directmedia Layer" от Mike Shah](https://www.youtube.com/playlist?list=PLvv0ScY6vfd-p1gSnbQhY7vMe2rng0IL0).

Направил е и [хранилище с ръководства в github](https://github.com/MikeShah/SDL2_Tutorials), които ми помогнаха да се уверя, че SDL2 е инсталирана на операционната ми система, преди да започна да програмирам.